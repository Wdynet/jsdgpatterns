/**
 * 节流模式：对重复的业务逻辑进行节流控制，执行最后一次操作并取消其他操作，以提高性能
 */
// 节流器
var throttle = function () {
    // 获取第一个参数
    var isClear = arguments[0], fn;
    // 如果第一个参数是Boolean类型，那么第一个参数则表示是否清楚定时器
    if(typeof isClear === 'boolean') {
        // 第二个参数则为函数
        fn = arguments[1];
        // 函数的计时器句柄存在，则清楚该定时器
        fn._throttleID && clearTimeout(fn._throttleID);
        // 否则通过定时器延迟函数的执行
    } else {
        // 第一个参数为函数
        fn = isClear;
        // 第二个参数为函数执行时的参数
        param = arguments[1];
        // 对执行时的参数适配默认值,这里我们用到以前学过的extend方法
        var p = extend({
            context: null, // 执行函数执行时的作用域
            args: [],   // 执行函数执行时的相关参数 （IE下要为数组）
            time: 300,   // 执行函数要延迟执行的事件
        }, param);
        // 清楚执行函数定时器句柄
        arguments.callee(true, fn);
        // 为函数绑定计时器句柄，延迟执行函数
        fn._throttleID = setTimeout(() => {
            // 执行函数
            fn.apply(p.context, p.args)
        }, p.time);
    }
}

/**
 * 构造节流器的思路是这样的：首先节流器能做两件事，第一件事情就是清楚将要执行的函数，此时要对节流器传递两个参数（是否清楚，执行函数），如果第一个参数为true，
 * 则表示清除将要执行的函数。同时会判断第二个参数（执行函数）有没有计时器句柄，有则清除计时器。
 * 节流器能做的第二件事情就是延迟执行函数。此时要传递两个参数（执行函数，相关参数）。在节流器内部首先要为执行函数绑定一个计时器句柄，来保存该执行函数的计时器，
 * 对于第二个参数 —— 相关参数来说，大致包括3个部分，执行函数在执行时的作用域、执行函数的参数、执行函数延迟执行的时间。
 */

// 滚动条返回顶部按钮，优化
// 假设引用了jQuery.js和easing.js方便返回顶部动画实现
function moveScroll() {
    var top = $(document).scrollTop();
    $('#back').animate({top: top + 300}, 400, 'easeOutCubic');
}
// 监听页面滚动条事件
$(window).on('scroll', function () {
    // 节流执行返回顶部按钮动画
    throttle(moveScroll);
})

// 当鼠标移动到微信或者微博icon的时候，显示出对应的二维码浮层
// 外观模式封装获取元素方法
function $(id) {
    return document.getElementById(id)
}
function $tag(tag, container) {
    container = container || document;
    return container.getElementsByTagName(tag);
}
// 浮层类
var Layer = function (id) {
    // 获取容器
    this.container = $(id);
    // 获取容器中的浮层容器
    this.layer = $tag('div', this.container)[0];
    // 获取icon容器
    this.lis = $tag('li', this.container);
    // 获取二维码图片
    this.imgs = $tag('img', this.container);
    // 绑定事件
    this.bindEvent();
}
Layer.prototype = {
    /**
     * 绑定交互事件
     * 对于交互部分，当鼠标光标划入容器，为接触用户误划入操作，浮层不应立即展现，此时应该使用节流器延迟浮层展现方法的执行。而鼠标光标移出容器的时间比较短的时候，可能是
     * 用户不小心移出的，此时应该使用节流器延迟隐藏浮层。
     */
    bindEvent: function () {
        // 缓存当前对象
        var that = this;
        // 隐藏浮层
        function hideLayer() {
            that.layer.className = '';
        }
        // 显示浮层
        function showLayer() {
            that.layer.className = 'show';
        }
        // 鼠标光标移入事件
        that.on(that.container, 'mouseenter', function () {
            // 清楚隐藏浮层方法计时器
            throttle(true, hideLayer);
            // 延迟执行浮层显示方法
            throttle(showLayer)
        }).on(that.container, 'mouseleave', function () {
            // 延迟浮层隐藏方法
            throttle(hideLayer);
            // 清楚显示浮层方法计时器
            throttle(true, showLayer)
        });

        // 遍历icon绑定事件
        for(var i = 0; i < that.lis.length; i++) {
            // 自定义属性index
            that.lis[i].index = i;
            // 为每一个li元素绑定鼠标移入事件
            that.on(that.lis[i], 'mouseenter', function () {
                // 获取自定义属性index
                var index = this.index;
                // 排除所有img的show类
                for(var j = 0; j < that.imgs.length; j++) {
                    that.imgs[j].className = "";
                }
                // 为目标图片设置show类
                that.imgs[index].className = 'show';
                // 从新定义浮层位置
                that.layer.style.left = -22 + 60 + index + 'px';
            })
        }
    },

    // 事件绑定方法
    on: function (ele, type, fn) {
        ele.addEventListener ? ele.addEventListener(type, fn, false) : ele.attachEvent('on' + type, fn);
        return this
    }
}

/**
 *  节流延迟加载图片类 *
 * @param {*} id 延迟加载图片的容器id
 * 注；图片格式如下<img src="img/loading.gif" data-src="img/1.jpg">
 */
function LazyLoad(id) {
    // 获取需要节流延迟加载图片的容器
    this.container = document.getElementById(id);
    // 缓存图片
    this.imgs = this.getImgs();
    // 执行逻辑
    this.init();
}
LazyLoad.prototype = {
    // 起始执行逻辑
    init: function () {
        // 加载当前视图图片
        this.update();
        // 绑定事件
        this.bindEvent();
    },

    // 获取延迟加载图片
    getImgs: function () {
        // 新数组容器
        var arr = [];
        // 获取图片
        var imgs = this.container.getElementsByTagName('img');
        // 将获取的图片转化为数组
        for(var i = 0, len = imgs.length; i < len; i++) {
            arr.push(imgs)
        }
        return arr;
    },

    /**
     * 加载图片方法，需要需要遍历每一个图片元素，如果处在可视区域内则加载并将其在图片缓存中清除。
     * **/ 
    update: function () {
        // 如果图片都加载完成，返回
        if(!this.imgs.length) {
            return;
        }
        // 获取图片长度
        var i = this.imgs.length;
        // 遍历图片
        for(--i; i > 0; i--) {
            // 如果图片在可是范围内
            if(this.shouldShow[i]) {
                // 加载图片
                this.imgs[i].src = this.imgs[i].getAttribute('data-src');
                // 清除缓存中的此图片
                this.imgs.splice(i, 1)
            }
        }
    },

    /**
     * 筛选需加载的图片 —— 判断图片是否在可视范围内
     * **/ 
    shouldShow: function (i) {
        // 获取当前图片
        var img = this.imgs[i],
        // 可视范围内顶部高度（页面滚动条top值）
        scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
        // 可视范围内底部高度
        scrollBottom = scrollTop + document.documentElement.clientHeight,
        // 图片的顶部位置
        imgTop = this.pageY(img),
        // 图片的底部位置
        imgBottom = imgTop + img.offsetHeight;

        /**
         * 判断图片是否在可视范围内，图片底部高度大于可视视图顶部高度并且图片底部高度小于可视视图底部高度，或者图片顶部高度大于可视视图顶部高度
         * 并且图片顶部高度小时可视视图底部高度
         */
        if(imgBottom > scrollTop && imgBottom < scrollBottom || (imgTop > scrollTop && imgTop < scrollBottom)) {
            return true
        }
        // 不满足上面条件则返回false
        return false
    },

    // 获取纵坐标位置
    pageY: function (element) {
        // 如果元素有父元素
        if(element.offsetParent) {
            // 返回元素 + 父元素高度
            return element.offsetTop + this.pageY(element.offsetParent);
        } else {
            // 否则返回元素高度
            return element.offsetTop;
        }
    },

    // 绑定事件（简化版）
    on: function (element, type, fn) {
        if(element.addEventListener) {
            addEventListener(type, fn, false);
        } else {
            element.attachEvent('on' + type, fn, false)
        }
    },

    /**
     * 节流器优化加载 —— 为窗口绑定resize事件与scroll事件
     * **/ 
    bindEvent: function () {
        var that = this;
        this.on(window, 'resize', function () {
            // 节流处理更新图片逻辑
            throttle(that.update, {context: that});
        });
        this.on(window, 'scroll', function () {
            // 节流处理更新图片逻辑
            throttle(that.update, {context: that});
        })
    }
}
// 最后实例化lazyLoad类来延迟加载container容器内的所有图片，并通过节流器实现加载优化
new LazyLoad('container');

/** 
 * 统计打包：
 *  节流模式优化了图片加载时的用户体验，对于一些前后端的数据请求有时也可以通过节流模式打包来优化请求次数。
 *  比如在统计中（尤其对于鼠标移入移出等频发性事件），我们经常监听事件触发次数，当触发次数达到某一值时才发送请求。
 *  **/

//  打包统计对象
var LogPack = function () {
    var data = [],   // 请求缓存数组
    MaxNum = 10,     // 请求缓存最大值
    itemSplitStr = "|",     // 统计项统计参数间隔符
    keyValueSplitStr = "*",     // 统计项统计参数键值对间隔符
    img = new Image();      // 请求触发器，通过图片src属性实现简单的get请求

    // 发送请求方法
    function sendLog() {
        // 请求参数
        var logStr = "";
        // 截取MaxNum个统计项发送
        var fireData = data.splice(0, MaxNum);
        // 遍历统计项
        for(var i = 0, len = fireData.length; i < len; i++) {
            // 添加统计项顺序索引
            logStr += 'log' + i + '=';
            // 遍历统计项内的统计参数
            for(var j in fireData[i]) {
                // 添加统计项参数键 + 间隔符 + 值
                logStr += j + keyValueSplitStr + fireData[i][j];
                // 添加统计项统计参数间隔符
                logStr += itemSplitStr;
            }
            // &符拼接多个统计项
            logStr = logStr.replace(/\|$/, '') + '&';
        }
        // 添加统计项打包长度
        logStr += 'logLength=' + len;
        // 请求触发器发送统计
        img.src = 'a.gif?' + logStr;
    }

    

    // 统计方法
    return function (param) {
        // 如果无参数则发送统计
        if(!param) {
            sendLog();
            return;
        }
        // 添加统计项
        data.push(param);
        // 如果统计项大于请求缓存最大值则发送统计请求包
        data.length >= MaxNum && sendLog();
    }
} ();

