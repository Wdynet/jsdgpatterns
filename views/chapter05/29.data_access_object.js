/**
 *  数据访问对象模式：抽象和封装对数据源的访问与存储，DAO通过对数据源链接的管理方便对数据的访问与存储.
 */

/**
 * 本地存储类
 * @param {*} preId 本地存储数据库前缀
 * @param {*} timeSign  时间戳与存储数据之间的拼接符
 */
var BaseLocalStorage = function (preId, timeSign) {
    //  定义本地存储数据库前缀
    this.preId = preId;
    // 定义时间戳与本地存储数据之间的拼接符
    this.timeSign = timeSign || '|-|';
}
// 数据操作状态 —— 本地存储类原型方法
BaseLocalStorage.prototype = {
    // 操作状态
    status: {
        SUCCESS: 0,   // 成功
        FAILURE: 1,   // 失败
        OVERFLOW: 2,   // 溢出
        TIMEOUT: 3    // 过期
    },
    // 保存本地存储链接
    storage: localStorage || window.localStorage,
    // 获取本地存储数据库数据真实字段
    getKey: function (key) {
        return this.preId + key;
    },
    
    /**
     * 添加（修改）数据
     * @param {*} key       数据字段标识
     * @param {*} value     数据值
     * @param {*} callback  回调函数
     * @param {*} time      添加时间
     */
    set: function (key, value, callback, time) {
        // 默认操作状态时成功
        var status = this.status.SUCCESS,
        // 获取真实字段
        key = this.getKey(key);
        try{
            // 参数为时间参数时获取时间戳
            time = new Date(time).getTime() || time.getTime()
        } catch(e) {
            // 为传入时间参数或者时间参数有误获取默认时间：一个月
            time = new Date().getTime() + 1000 * 60 * 60 * 24 * 31;
        }

        try {
            // 向数据库中添加数据
            this.storage.setItem(key, time + this.timeSign + value)
        } catch(e) {
            // 溢出失败，返回溢出状态
            status = this.status.OVERFLOW;
        }
        // 有回调函数则执行回调函数并传入参数操作状态，真实数据字段标识以及存储数据值
        callback && callback.call(this, status, key, value);
    }, 

    /**
     *  获取数据
     * @param {*} key
     * @param {*} callback
     */
    get: function (key, callback) {
        // 默认操作状态时成功
        var status = this.status.SUCCESS,
        // 获取
        key = this.getKey(key),
        // 默认值为空
        value = null,
        // 时间戳与存储数据之间的拼接符长度
        timeSignLen = this.timeSign.length,
        // 缓存当前对象
        that = this,
        // 时间戳与存储数据之间的拼接符起始位置
        index,
        // 时间戳
        time,
        // 最终获取的数据
        result;

        try {
            // 获取字段对应的数据字符串
            value = that.storage.getItem(key)
        } catch(e) {
            // 获取失败则返回失败状态，数据结果为null
            result = {
                status : that.status.FAILURE,
                value: null
            }
            // 执行回调并返回
            callback && callback.call(this, result.status, result.value);
            return result
        }
        // 如果成功获取数据字符串
        if(value) {
            // 获取时间戳与存储数据之间的拼接符起始位置
            index = value.indexOf(that.timeSign);
            // 获取时间戳
            time = +value.slice(0, index)
            // 如果时间为过期
            if(new Date(time).getTime() > new Date().getItem() || time == 0) {
                // 获取数据结果（拼接符后面的字符串）
                value = value.slice(index + timeSignLen)
            } else {
                // 过期则结果为null
                value = null,
                // 设置状态为过期状态
                status = that.status.TIMEOUT;
                // 删除该字段
                that.remove(key)
            }
        } else {
            // 未获取数据字符串状态为失败状态
            status = that.status.FAILURE;
        }
        // 设置结果
        result = {
            status: status,
            value: value
        }
        // 执行回调函数
        callback && callback.call(this, result.status, result.value);
        return result;
    },

    /**
     *  删除数据
     * @param {*} key
     * @param {*} callback
     */
    remove: function (key, callback) {
        // 设置默认操作状态为失败
        var status = this.status.FAILURE,
        // 获取实际数据字段名称
        key = this.getKey(key),
        // 设置默认数据结果为空
        value = null;

        try {
            // 获取字段对应的数据
            value = this.storage.getItem(key)
        } catch(e) {}

        // 如果数据存在
        if(value) {
            try {
                // 删除数据
                this.storage.removeItem(key);
                // 设置操作成功
                status = this.status.SUCCESS;
            } catch(e) {}
        }
        // 执行销毁，注意传入回调函数中的数据值，如果操作状态成功则返回真实的数据结果，否则返回空
        callback && callback.call(this, status, status > 0 ? null : value.slice(value.indexOf(this.timeSign) + this.timeSign.length))
    }
}

// 检验DAO
var LS = new BaseLocalStorage('LS_');
LS.set('a', 'xiao ming', function () { console.log(arguments) })
LS.get('a', function () { console.log(arguments) })
LS.remove('a', function () { console.log(arguments) })
LS.remove('a', function () { console.log(arguments) })
LS.get('a', function () { console.log(arguments) })

// MongoDB
// 其实数据访问对象模式更适合与服务器的数据库操作，比如我们在nodejs中操作MongoDB时，便可创建MongoDB数据库的数据访问对象
// 在创建MongoDB数据库的数据访问对象之前我们先做些准备工作，比如我们将应用的数据库名、主机名、端口号等下载配置文件中方便日后使用。
/** config.js **/
// 将配置数据输出
module.exports = {
    // 数据库相关配置数据
    DB: {
        db: 'demo',             // 数据库名称
        host: 'localhost',      // 主机号
        port: 27017,            // 端口号
    }
}

/** db.js **/
// 引用mongodb模块
var mongdb = require('mongodb');
// 引用配置模块的数据库配置信息
var config = require('./config').DB;
// 创建数据库对象
var d = new mongdb.Db(
    config.db,                      // 数据库名称
    new mongdb.Server(              
        config.host,                // 主机号
        config.port,                // 端口号
        {auto_reconnect : true}     // 自动连接
    ),
    {safe: true}                    // 安全模式
)
// 输出数据访问对象
exports.DB = function (col) {
    // 我们要执行的增删改查操作要在connect中的fn中实现
    return {
        /**
         * 插入数据 *
         * @param {*} data      插入数据项
         * @param {*} success   操作成功回调函数
         * @param {*} fail      操作失败回调函数
         */
        insert: function (data, success, fail) {
            // 打开数据库操作col集合
            connect(col, function (col, db) {
                // 向集合中插入数据
                col.insert(data, function (err, docs) {
                    // 失败，抛出插入错误
                    if(err) {
                        fail && fail(err)
                    } else {
                        // 成功
                        success && success(docs)
                    }
                    // 关闭数据库
                    db.close();
                })
            })
        },
        /**
         * 删除数据 *
         * @param {*} data      删除数据项
         * @param {*} success   操作成功回调函数
         * @param {*} fail      操作失败回调函数
         */
        remove: function (data, success, fail) {
            // 打开数据库操作col集合
            connect(col, function (col, db) {
                // 在集合中删除数据项
                col.remove(data, function (err, len) {
                    if(err) 
                        fail && fail(err);
                    else 
                        success && success(len);
                    db.close();
                })
            })
        },
        /**
         * 更新数据 *
         * @param {*} con      筛选条件
         * @param {*} doc      更新数据项
         * @param {*} success   操作成功回调函数
         * @param {*} fail      操作失败回调函数
         */
        update: function(con, doc, success, fail) {
            connect(con, doc, function (err, len) {
                if(err)
                    fail && fail(err);
                else 
                    success && success(len);
                db.close();
            })
        },

        /**
         * 查找操作 *
         * @param {*} con      查找条件
         * @param {*} success   操作成功回调函数
         * @param {*} fail      操作失败回调函数
         */
        find: function (con, success, fail) {
            connect(col, function (col, db) {
                // 在集合中查找数据
                col.find(con).toArray(function (err, docs) {
                    if(err) 
                        fail && fail(err);
                    else 
                        success && success(docs);
                    db.close();
                })
            })
        },
    }
}

/**
 * 打开数据库，操作集合
 *
 * @param {*} col 集合名
 * @param {*} fn  操作方法
 */
function connect(col, fn) {
    // 打开数据库
    d.open(function (err, db) {
        // 打开数据库报错则跑出错误
        if(err) {
            throw err;
        } else {
            db.collection(col, function (err, col) {
                // 操作集合出错则抛出错误
                if(err) {
                    throw err
                } else {
                    fn && fn(col, db);
                }
            })
        }
    })
}

/** test.js **/
var DB = require('./db').DB;    //引用数据访问对象模块
var user = DB('user');     // 操作user集合
// 向集合中插入一条数据
user.insert({name: 'Tommy', nick: 'letter fish'}, function (docs) {
    console.log(docs)  
    // {name: 'Tommy', nick: 'letter fish', _id: 45154e12a45} 【id为数据项的索引值】
})

user.remove({name: '小白'}, function (len) {
    console.log(len)  // 1  删除数据项长度
})

user.remove({name: '小红', nick: '红红'}, function (len) {
    console.log(len)  // 1
})

user.find({name: '小黑'}, function (doc) {
    console.log(doc)
})

// 有了数据访问对象DB，我们在操作其他集合时变得容易，比如我们操作book时，直接在test.js文件中添加如下代码即可
var book = DB('book');
book.insert({title: 'JavaScript设计模式', taype: 'JavaScript'})