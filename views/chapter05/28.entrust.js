/**
 *  委托模式：多个对象接受并同一处理请求，他们将请求委托给另一个对象统一处理请求。
 *
 *  如果给每一个li都添加绑定事件，会让内存消耗变大，尤其是一些老版本浏览器IE6、IE7、IE8，事件过多会影响用户体验的。
 *  所以又可以通过代理模式，绑定一个时间完成需求
 */
var ul = document.getElementById('container');
// 委托父元素
ul.onclick = function (e) {
    var e = e || window.event,
    tar = e.target || e.srcElement;
    if(tar.nodeName.toLowerCase() === 'li') {
        tar.style.backgroundColor = 'grey';
    }
}

var article = document.getElementById('article');
article.onclick = function () {
    var e = e || window.event,
    tar = e.target || e.srcElement;
    if(tar.nodeName.toLowerCase() === "p") {
        tar.innerHTML = '我要更改这段内容';
    }
}
var p = document.createElement('p');
p.innerHTML = '新增一端内容';
article.appendChild(p);

/**
 *  委托模式还能处理一些内存外泄问题，这又要提到糟糕的老版本IE浏览器了，由于他的引用计数式垃圾回收机制，使得那些对dom元素的引用没有显性清除的数据会遗留在内存中，
 *  除非关闭浏览器，否则无法清除
 */
var g = function (id) { return document.getElementById(id) }
g('btn').onclick = function () {
    g('btn') = null;  // 显性清除该绑定事件,但这个清除语句在一些标准浏览器中是不需要的
    g('btn_container').innerHTML = '触发了事件';
}
// 标记清除方式管理自身的内存，更好的解决方式便是委托模式了
g('btn_container').onclick = function (e) {
    // 获取触发事件元素
    var target = e && e.target || window.event.srcElement;
    // 判断触发事件元素是否是id为btn的元素
    if(target.id === 'btn') {
        // 重置父元素内容
        g('btn_container').innerHTML = '触发了事件';
    }
}

// 数据分发
$.get('./deal.php?q=banner', function (res) {
    // 处理banner模块逻辑
})
$.get('./deal.php?q=aside', function (res) {
    // 处理aside模块逻辑
})
$.get('./deal.php?q=member', function (res) {
    // 处理member模块逻辑
})
/**
 * ... 这样做会想服务器发送多次请求，既是对资源的浪费，又会造成漫长的等待。最好的方式是将这些请求打包，委托以另外一个对象发送，
 * 当得到响应数据时在通过委托对象拆包数据分发给各个模块
 * **/ 
var Deal = {
    banner: function (res) {
        // 处理banner模块逻辑
    },
    asice: function (res) {
        // 处理asice模块逻辑
    },
    member: function (res) {
        // 处理member模块逻辑
    }
}
$.get('./deal.php?', function (res) {
    // 数据拆包分发
    for(var i in res) {
        Deal[i] && Deal[i](res[i])
    }
})
// 这样我们就通过一个请求便可将5次请求业务一次性做完，这既节省流量又节约了多次请求对时间的开销