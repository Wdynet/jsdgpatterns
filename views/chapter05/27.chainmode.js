/**
 * 链模式：通过在对象方法中将当前对象返回，实现对同一个对象多个方法的链式调用。从而简化对该对象的多个方法的多次调用，或对该对象的多次引用。 
 */

// 原型式继承
var A1 = function () {  }
A1.prototype = {
    length: 2,
    size: function () {
        return this.length
    }
}
// 正确使用
var a = new A1();
console.log(a.size())

// 错误使用
console.log(A1.size())
console.log(A1().size())

// 不能直接调用，那就找位帮手
var A = function () {
    return BarProp;
}
var B = A.prototype = {
    length: 2,
    size: function () {
        return this.length
    }
}
console.log(A().size())  //2

// 而在jQuery中，为了减少变量的创建，索性直接将B对象看作是A的一个属性设置，并获取元素
var A = function (seletor) {
    // return A.fn.init(seletor);
    // 后获取的id会被覆盖掉，解决办法也很简单，使用new关键字复制创建即可，不过再调用size方法就报错了,这可以算是方法丢失
    return new A.fn.init(seletor)
}
A.fn = A.prototype = {
    // init: function (seletor) {
    //     return document.getElementById(seletor)
    // },
    // 强化构造器,那么调用new方法返回后再调用size就不会出错了
    constructor: A,
    init: function (seletor) {
        // console.log(this.constructor)
        this[0] = document.getElementById(seletor);
        this.length = 1;
        return this
    },
    length: 2,
    size : function () {
        return this.length
    }
}
A.fn.init.prototype = A.fn;

// console.log(A('demo'))  // <div id="demo"></div> 
console.log(A('demo').size())  // 1 


/**
 * 丰富元素获取 *
 * @param {*} selector 选择符
 * @param {*} context 上下文
 * @returns
 */
var A = function (selector, context) {
    return new A.fn.init(selector, context)
}
A.fn = A.prototype = {
    constructor: A,
    init: function (selector, context) {
        // 获取元素长度
        this.length = 0,
        // 默认获取元素的上下文为document
        context = context || document;

        // 如果是id选择符 按位非将-1转为0，转化为布尔值false
        if(~selector.indexOf('#')) {
            // 截取id并选择
            this[0] = document.getElementById(selector.slice(1));
            this.length = 1;
        } else {
            // 如果是元素名称
            var doms = context.getElementsByTagName(selector),
            i = 0,
            len = doms.length;
            for(; i < len; i++) {
                // 压入this中
                this[i] = doms[i]
            }
            // 校正长度
            this.length = len;
        }
        // 保存上下文
        this.context = context;
        // 保存选择符
        this.selector = selector;
        // 返回对象
        return this;
    },
    // 增强数组
    push: [].push,
    sort: [].sort,
    splice: [].splice
}

// 对象拓展
A.extend = A.fn.extend = function () {
    // 拓展对象从第二个参数算起
    var i = 1,
    len = arguments.length,
    // 第一个参数作为源对象
    target = arguments[0],
    // 拓展对象中的属性
    j;
    // 如果只传一个参数
    if(i == len) {
        // 源对象为当前对象
        target = this;
        // i从0计数
        i--;
    }
    // 遍历参数中拓展对象
    for(; i < len; i++) {
        // 遍历拓展对象中的属性
        for(j in arguments[i]) {
            // 拓展源对象
            target[i] = arguments[i][j];
        }
    }
    // 返回源对象
    return target;
}

// 测试用例
var demo = A.extend({first: 1}, {second: 2}, {third: 3});
console.log(demo)
// 拓展A.fn方式一
A.extend(A.fn, {version: '1.0'})
console.log(A('demo').version)

// 拓展A.fn方式二
A.fn.extend({getVersion: function () { return this.version }})
console.log(A('demo').getVersion())

// 拓展A方式一
A.extend(A, {author: '老张'});
console.log(A.author)
// 拓展A方式二
A.extend({nickname: '渔业清河'});
console.log(A.nickname)

// 添加方法
A.fn.extend({
    // 添加事件：
    on: (function () {
        // 标准浏览器DOM2级事件
        if(document.addEventListener) {
            return function (type, fn) {
                var i = this.length - 1;
                // 遍历所有元素添加事件
                for(; i >= 0; i--) {
                    this[i].addEventListener(type, fn, false)
                }
                // 返回源对象
                return this
            }
        } else if(document.attachEvent) {
            // IE浏览器DOM2级事件
            return function (type, fn) {
                var i = this.length - 1;
                for(; i >= 0; i--) {
                    this[i].addEvent('on' + type, fn)
                }
                return this
            }
        } else {
            // 不支持DOM2级事件浏览器添加事件
            return function (type, fn) {
                var i = this.length - 1;
                for(; i >= 0; i--) {
                    this[i]['on' + type] = fn;
                }
                return this
            }
        }
    })()
})

/**
 *  对于浏览器事件方法，我们针对不同浏览器采用不同的绑定方式，当然是为了日后绑定事件时减少浏览器功能校验开销，
 *  我们在第一次加载时创建出适用该浏览器的事件绑定方法。
 */
A.extend({
    // 将'-'分割线转化成驼峰式，如:'border-color' -> 'borderColor'
    camelCase: function (str) {
        return str.replace(/\-(\w)/g, function (all, letter) {
            return letter.toUpperCase();
        })
    }
});
A.extend({
    // 设置css样式
    css: function () {
        var arg = arguments,
        len = arg.length;
        if(this.length < 1) {
            return this
        }
        // 只有一个参数时
        if(len === 1) {
            // 如果为字符串则为获取第一个元素css样式
            if(typeof arg[0] === 'string') {
                // IE
                if(this[0].currentStyle) {
                    return this[0].currentStyle[name]
                } else {
                    return getComputedStyle(this[0], false)[name]
                }
            } else if(typeof arg[0] === 'object') {
                // 为对象时则设置多个样式
                for(var i in arg[0]) {
                    // 遍历每个样式
                    for(var j = this.length - 1; j >= 0; j--) {
                        // 调用拓展方法camelCase将'-'分割线转化为驼峰式
                        this[j].style[A.camelCase(i)] = arg[0][i];
                    }
                }
            } else if(len === 2) {
                // 两个参数则设置一个样式
                for(var j = this.length - 1; j >= 0; j--) {
                    this[j].style[A.camelCase(arg[0])] = arg[1]
                }
            }
            return this
        }
    }
})

A.fn.extend({
    // 设置属性
    attr: function () {
        var arg = arguments,
        len = arg.length;
        if(this.length < 1) {
            return this;
        }
        // 如果一个参数
        if(len === 1) {
            // 为字符串则获取第一个元素属性
            if(typeof arg[0] === 'string') {
                return this[0].getAttribute(arg[0]);
            } else if(typeof arg[0] === 'object') {
                // 为对象设置每个元素的多个属性
                for(var i in arg[0]) {
                    // 遍历属性
                    for(var j = this.length - 1; j >= 0; j--) {
                        this[j].setAttribute(i, arg[0][i])
                    }
                }
            }
        } else if(len === 2) {
            // 两个参数则设置每个元素单个属性
            for(var j = this.length - 1; j >= 0; j--) {
                this[j].setAttribute(arg[0], arg[1])
            }
        }
        return this
    }
})

A.fn.extend({
    // 获取或设置元素的内容
    html: function () {
        var arg = arguments,
        len = arg.length;
        // 无参数则获取第一个参数的内容
        if(len === 0) {
            return this[0] && this[0].innerHTML;
            // 一个参数则设置每一个元素的内容
        } else {
            for(var i = this.length - 1; i >= 0; i--) {
                this[i].innerHTML = arg[0]
            }
        }
        return this
    }
})

// 链式操作
A('div').css({
    height: '30px',
    border: '1px solid #eee',
    'background-color': 'red'
})
.attr('class', 'demo')
.html('add demo text')
.on('click', function () {
    console.log('clicked')
})