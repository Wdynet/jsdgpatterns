/**
 * 迭代器模式 —— 点钞机：在不暴露对象内部结构的同时，可以顺序地访问聚合对象内部的元素。
 * 
 * 在程序语言中循环语句是很重要的一种语句，然而有时程序中过多地出现循环语句又显得臃肿不堪，可读性差。所以就有了迭代器模式。
 * **/ 

var Iterator = function (items, container) {
    // 获取父容器，若container参数存在，并且可以获取该元素则获取，否则获取documen
    var container = container && document.getElementById(container) || document,
    // 获取元素
    items = container.getElementByTagName(items),
    // 获取元素长度
    length = items.length,
    // 获取当前索引值，默认：0
    index = 0;

    // 缓存源生数组splice方法
    var splice = [].splice;
    return {
        // 获取第一个元素，首先设置当前索引并缓存，然后返回索引对应的元素，最后一个元素的也是这个方法获取
        first: function () {
            index = 0;
            return items[index]
        },
        // 获取最后一个元素
        second: function () {
            index = length - 1;
            return items[index]
        },

        // 获取前一个元素,要判断这个元素是否为第一个，是的话则返回空元素，并设置索引为0
        prev: function () {
            if(--index > 0) {
                return items[index]
            } else {
                index = 0;
                return null;
            }
        },
        // 获取下一个元素
        next: function() {
            if(++index < length) {
                return items[index]
            } else {
                index = length - 1;
                return null
            }
        },

        /**
         * 获取某一个元素,需要注意的是，如果参数大于等于0则直接对length取模获取对应的元素并设置索引值，否则对length取模后还要加上length才能获取对应的索引值，此时是从后向前搜索元素。
         * **/ 
        get: function (num) {
            // 如果num大于等于0,再正向获取，否则逆向获取
            index = num >= 0 ? num % length : num % length + length;
            // 返回对应元素
            return items[index]
        },

        // 对每一个元素执行某一个方法
        dealEach: function (fn) {
            // 第二个参数开始为回调函数参数中参数
            var args = splice.call(arguments, 1);
            // 遍历元素
            for(var i = 0; i < length; i++) {
                // 对元素执行回调函数
                fn.applay(items[i], args)
            }
        },
        // 对每一个元素执行某一个方法
        dealItem: function (num, fn) {
            // 对元素执行回调函数，注：第三个参数开始为回调函数中参数 2 通过this.get方法设置index索引值
            fn.applay(this.get(num), splice.call(arguments, 2))
        },
        // 排他方式处理某一个元素
        exclusive: function (num, allFn, numFn) {
            // 对所有元素执行回调函数
            this.dealEach(allFn);
            // 如果num类型为数组
            if(Object.prototype.toString.call(num) == "[object Array]") {
                // 分别处理数组中每一个元素
                this.dealItem(num[i], numFn)
            } else {
                // 处理第n个元素
                this.dealItem(num, numFn)
            }
        },
    }
}

// container ul li 创建一个迭代器对象
var demo = new Iterator();
// 如果我们想为每一个元素处理一些逻辑，我们可以这样
console.log(demo.first()) 
console.log(demo.pre())
console.log(demo.next())
console.log(demo.get(2000))

// 处理所有元素
demo.dealEach(function(text, color) {
    this.innerHTML = text;
    this.style.background = color;
}, 'test', 'pink');

// 排他思想处理第3个与第四个元素
demo.exclusive([2, 3], function () {
    this.innerHTML = '被排除的';
    this.style.background = 'green';
}, function () {
    this.innerHTML = '选中的';
    this.style.background = 'red';
})

// 数组迭代器
var eachArray = function (arr, fn) {
    var i = 0,
    len = arr.length;
    // 遍历数组
    for(; i < len; i++) {
        // 依次执行回调函数，注意回调函数中传入的参数第一个为索引，第二个为该索引对应的值
        if(fn.call(arr[i], i, arr[i]) === false) {
            break
        }
    }
}

// 对象迭代器
var eachObject = function (obj, fn) {
    for(var i in obj) {
        // 依次执行回调函数，注意回调函数中传入的参数第一个为属性，第二个为该属性对应的值
        if(fn.call(obj[i], i, obj[i]) === false) {
            break;
        }
    }
}

// 试用迭代器
for(var arr = [], i = 0; i < 5; arr[i++] = i);
eachArray(arr, function (i, data) {
    console.log(i, data)
})

var obj = {
    a : 23,
    b : 56,
    c : 69,
}
eachObject(obj, function (i, value) {
    console.log(i, value)
})

// 同步变量迭代取值器
AGetter = function (key) {
    if(!A) 
        return undefined;
    var result = A;
    key = key.split('.');
    for(var i = 0, len = key.length; i < len; i++) {
        if(result[key[i]] !== undefined) {
            result = result[key[i]]
        } else {
            return undefined
        }
    }

    return result
}

// 分支循环嵌套问题
window.onload = function () {
    var canvas = document.getElementsByTagName('canvas')[0]; // 获取画布
    var img = document.images[0];  // 获取图片
    var width = (canvas.width = img.width * 2) / 2; // 获取并设置宽度
    var height = canvas.height = img.height;  // 获取并设置高度
    var ctx = canvas.getContext('2d');  // 获取渲染上下文

    ctx.drawImage(img, 0, 0);

    /**
     *  在处理图片数据时需要两个步骤，第一步是迭代每一个图片像素数据，第二步是根据给定特效类型选择不同算法处理像素数据。
     *  param t 特效类型
     *  param x x坐标
     *  param y y坐标
     *  param w 宽度
     *  param h 高度
     *  param a 透明度
     */
    function dealImage(t, x, y, w, h, a) {
        // 获取画布图片数据
        var canvasData = ctx.getImageData(x, y, w, h);
        // 获取像素数据
        var data = canvasData.data;
        // 遍历每组像素数据（并非是最优的分支极多的情况下，这种性能不稳定）
        /** for(var i = 0, len = data.len; i < len; i += 4) {
            switch(t) {
                // 红色滤镜 将绿色与蓝色取值为0
                case 'red':
                    data[i + 1] = 0;
                    data[i + 2] = 0;
                    data[i + 3] = a;
                    break;
                // 绿色滤镜 将红色和蓝色取值为0
                case 'green':
                    data[i] = 0;
                    data[i + 2] = 0;
                    data[i + 3] = a;
                    break;
                // 蓝色滤镜 将红色和绿色取值为0
                case 'blue':
                    data[i] = 0;
                    data[i + 1] = 0;
                    data[i + 3] = a;
                    break;
                // 平均值灰色滤镜 取三色平均值
                case 'gray':
                    var num = parseInt((data[i] + data[i + 1] + data[i + 2]) / 3);
                    data[i] = num;
                    data[i + 1] = num;
                    data[i + 2] = num;
                    data[i + 3] = a;
                    break;
                // 其他方案
            }
        }  **/
        
        // 最优解决方案，策略模式和迭代器模式的综合运用
        var Deal = function () {
            var method = {
                // 默认类型 —— 平均灰度特效
                'default' : function (i) {
                    return method['gray'](i)
                },
                // 红色特效
                'red' : function (i) {
                    data[i + 1] = 0;
                    data[i + 2] = 0;
                    data[i + 3] = a;
                },
                // 平均灰度特效
                'gray' : function (i) {
                    data[i] = data[i + 1] = parseInt(data[i + 2] = (data[i] + data[i + 1] + data[i + 2]) / 3) ;
                    data[i + 3] = a;
                }
            }
            // 主函数，通过给定类型返回对应滤镜算法
            return function (type) {
                return method[type] || method["default"];
            }
        }();
        // 迭代器处理数据
        function eachData(fn) {
            for(var i = 0, len = data.length; i < len; i+= 4) {
                // 处理一组像素数据
                fn(i)
            }
        }
        // 处理数据
        eachData(Deal(t))

        // 绘制处理后的图片
        ctx.putImageData(canvasData, width + x, y)
    }


    // 为图片添加特效
    dealImage('gray', 0, 0, width, height, 255);
    dealImage('gray', 100, 50, 300, 200, 100);
    dealImage('gray', 150, 100, 200, 100, 255);
}



